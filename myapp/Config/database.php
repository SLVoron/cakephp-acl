<?php
class DATABASE_CONFIG {

	public $cakephpacl = array(
		'datasource' => 'Database/Mysql',
		'persistent' => true,
		'host' => 'localhost',
		'login' => 'root',
		'password' => '',
		'database' => 'cakephpacl',
	);
}
